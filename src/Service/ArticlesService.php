<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 07/06/20
 * Time: 21:56
 */

namespace App\Service;


use App\Repository\ArticlesRepository;

class ArticlesService
{
    protected $articleRepository;

    public function __construct(ArticlesRepository $articlesRepository)
    {
        $this->articleRepository = $articlesRepository;
    }

    public function getAllArticle()
    {
        $articles = $this->articleRepository->findAll([], ['created_at' => 'desc']);
        return $articles;
    }

    public function getArticleBySlug($slug)
    {
        $article = $this->articleRepository->findOneBy(['slug'=> $slug]);
        return $article;
    }
}