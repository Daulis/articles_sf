(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["article"],{

/***/ "./assets/js/article.js":
/*!******************************!*\
  !*** ./assets/js/article.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js */ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js");
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3__);




var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

var routes = __webpack_require__(/*! ../../public/js/fos_js_routes */ "./public/js/fos_js_routes.json");


_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3___default.a.setRoutingData(routes);
console.log(_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3___default.a.generate('actualites_articles'));

var axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");

var $articleContainer = $('#article-container');

function showArticles(data) {
  data.forEach(function (article) {
    var newContent = "<h1><a href=\"".concat(article.url, "\">").concat(article.title, "</a></h1><p>").concat(article.content, "</p>");
    $articleContainer.append(newContent);
  });
}

function downloadArticles() {
  $.ajax({
    type: 'GET',
    url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_3___default.a.generate('actualites_hello'),
    dataType: 'json',
    success: function success(resp) {
      showArticles(resp);
    },
    error: function error() {
      console.log('error');
    }
  });
}

$(document).ready(function () {
  /*$('#btn-show-article').on('click', function () {
      downloadArticles();
  })*/
  downloadArticles();
});

/***/ }),

/***/ "./public/js/fos_js_routes.json":
/*!**************************************!*\
  !*** ./public/js/fos_js_routes.json ***!
  \**************************************/
/*! exports provided: base_url, routes, prefix, host, port, scheme, locale, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"base_url\":\"\",\"routes\":{\"actualites_articles\":{\"tokens\":[[\"text\",\"/actualites/\"]],\"defaults\":[],\"requirements\":[],\"hosttokens\":[],\"methods\":[],\"schemes\":[]},\"actualites_hello\":{\"tokens\":[[\"text\",\"/actualites/hello\"]],\"defaults\":[],\"requirements\":[],\"hosttokens\":[],\"methods\":[],\"schemes\":[]}},\"prefix\":\"\",\"host\":\"localhost\",\"port\":\"\",\"scheme\":\"http\",\"locale\":[]}");

/***/ }),

/***/ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js":
/*!************************************************************************************!*\
  !*** ./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.join */ "./node_modules/core-js/modules/es.array.join.js");

__webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.freeze */ "./node_modules/core-js/modules/es.object.freeze.js");

__webpack_require__(/*! core-js/modules/es.object.keys */ "./node_modules/core-js/modules/es.object.keys.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.regexp.constructor */ "./node_modules/core-js/modules/es.regexp.constructor.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (e, t) {
  var n = t();
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (n.Routing),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function () {
  "use strict";

  function e(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }

  var t = Object.assign || function (e) {
    for (var t = 1; t < arguments.length; t++) {
      var n = arguments[t];

      for (var o in n) {
        Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o]);
      }
    }

    return e;
  },
      n = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
    return _typeof(e);
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : _typeof(e);
  },
      o = function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var o = t[n];
        o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
      }
    }

    return function (t, n, o) {
      return n && e(t.prototype, n), o && e(t, o), t;
    };
  }(),
      r = function () {
    function r(t, n) {
      e(this, r), this.context_ = t || {
        base_url: "",
        prefix: "",
        host: "",
        port: "",
        scheme: "",
        locale: ""
      }, this.setRoutes(n || {});
    }

    return o(r, [{
      key: "setRoutingData",
      value: function value(e) {
        this.setBaseUrl(e.base_url), this.setRoutes(e.routes), "prefix" in e && this.setPrefix(e.prefix), "port" in e && this.setPort(e.port), "locale" in e && this.setLocale(e.locale), this.setHost(e.host), this.setScheme(e.scheme);
      }
    }, {
      key: "setRoutes",
      value: function value(e) {
        this.routes_ = Object.freeze(e);
      }
    }, {
      key: "getRoutes",
      value: function value() {
        return this.routes_;
      }
    }, {
      key: "setBaseUrl",
      value: function value(e) {
        this.context_.base_url = e;
      }
    }, {
      key: "getBaseUrl",
      value: function value() {
        return this.context_.base_url;
      }
    }, {
      key: "setPrefix",
      value: function value(e) {
        this.context_.prefix = e;
      }
    }, {
      key: "setScheme",
      value: function value(e) {
        this.context_.scheme = e;
      }
    }, {
      key: "getScheme",
      value: function value() {
        return this.context_.scheme;
      }
    }, {
      key: "setHost",
      value: function value(e) {
        this.context_.host = e;
      }
    }, {
      key: "getHost",
      value: function value() {
        return this.context_.host;
      }
    }, {
      key: "setPort",
      value: function value(e) {
        this.context_.port = e;
      }
    }, {
      key: "getPort",
      value: function value() {
        return this.context_.port;
      }
    }, {
      key: "setLocale",
      value: function value(e) {
        this.context_.locale = e;
      }
    }, {
      key: "getLocale",
      value: function value() {
        return this.context_.locale;
      }
    }, {
      key: "buildQueryParams",
      value: function value(e, t, o) {
        var r = this,
            i = void 0,
            u = new RegExp(/\[\]$/);
        if (t instanceof Array) t.forEach(function (t, i) {
          u.test(e) ? o(e, t) : r.buildQueryParams(e + "[" + ("object" === ("undefined" == typeof t ? "undefined" : n(t)) ? i : "") + "]", t, o);
        });else if ("object" === ("undefined" == typeof t ? "undefined" : n(t))) for (i in t) {
          this.buildQueryParams(e + "[" + i + "]", t[i], o);
        } else o(e, t);
      }
    }, {
      key: "getRoute",
      value: function value(e) {
        var t = this.context_.prefix + e,
            n = e + "." + this.context_.locale,
            o = this.context_.prefix + e + "." + this.context_.locale,
            r = [t, n, o, e];

        for (var i in r) {
          if (r[i] in this.routes_) return this.routes_[r[i]];
        }

        throw new Error('The route "' + e + '" does not exist.');
      }
    }, {
      key: "generate",
      value: function value(e, n) {
        var o = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
            i = this.getRoute(e),
            u = n || {},
            s = t({}, u),
            c = "",
            a = !0,
            l = "",
            f = "undefined" == typeof this.getPort() || null === this.getPort() ? "" : this.getPort();

        if (i.tokens.forEach(function (t) {
          if ("text" === t[0]) return c = r.encodePathComponent(t[1]) + c, void (a = !1);
          {
            if ("variable" !== t[0]) throw new Error('The token type "' + t[0] + '" is not supported.');
            var n = i.defaults && t[3] in i.defaults;

            if (!1 === a || !n || t[3] in u && u[t[3]] != i.defaults[t[3]]) {
              var o = void 0;
              if (t[3] in u) o = u[t[3]], delete s[t[3]];else {
                if (!n) {
                  if (a) return;
                  throw new Error('The route "' + e + '" requires the parameter "' + t[3] + '".');
                }

                o = i.defaults[t[3]];
              }
              var l = !0 === o || !1 === o || "" === o;

              if (!l || !a) {
                var f = r.encodePathComponent(o);
                "null" === f && null === o && (f = ""), c = t[1] + f + c;
              }

              a = !1;
            } else n && t[3] in s && delete s[t[3]];
          }
        }), "" === c && (c = "/"), i.hosttokens.forEach(function (e) {
          var t = void 0;
          return "text" === e[0] ? void (l = e[1] + l) : void ("variable" === e[0] && (e[3] in u ? (t = u[e[3]], delete s[e[3]]) : i.defaults && e[3] in i.defaults && (t = i.defaults[e[3]]), l = e[1] + t + l));
        }), c = this.context_.base_url + c, i.requirements && "_scheme" in i.requirements && this.getScheme() != i.requirements._scheme) {
          var h = l || this.getHost();
          c = i.requirements._scheme + "://" + h + (h.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c;
        } else if ("undefined" != typeof i.schemes && "undefined" != typeof i.schemes[0] && this.getScheme() !== i.schemes[0]) {
          var p = l || this.getHost();
          c = i.schemes[0] + "://" + p + (p.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c;
        } else l && this.getHost() !== l + (l.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) ? c = this.getScheme() + "://" + l + (l.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c : o === !0 && (c = this.getScheme() + "://" + this.getHost() + (this.getHost().indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c);

        if (Object.keys(s).length > 0) {
          var d = void 0,
              y = [],
              v = function v(e, t) {
            t = "function" == typeof t ? t() : t, t = null === t ? "" : t, y.push(r.encodeQueryComponent(e) + "=" + r.encodeQueryComponent(t));
          };

          for (d in s) {
            this.buildQueryParams(d, s[d], v);
          }

          c = c + "?" + y.join("&");
        }

        return c;
      }
    }], [{
      key: "getInstance",
      value: function value() {
        return i;
      }
    }, {
      key: "setData",
      value: function value(e) {
        var t = r.getInstance();
        t.setRoutingData(e);
      }
    }, {
      key: "customEncodeURIComponent",
      value: function value(e) {
        return encodeURIComponent(e).replace(/%2F/g, "/").replace(/%40/g, "@").replace(/%3A/g, ":").replace(/%21/g, "!").replace(/%3B/g, ";").replace(/%2C/g, ",").replace(/%2A/g, "*").replace(/\(/g, "%28").replace(/\)/g, "%29").replace(/'/g, "%27");
      }
    }, {
      key: "encodePathComponent",
      value: function value(e) {
        return r.customEncodeURIComponent(e).replace(/%3D/g, "=").replace(/%2B/g, "+").replace(/%21/g, "!").replace(/%7C/g, "|");
      }
    }, {
      key: "encodeQueryComponent",
      value: function value(e) {
        return r.customEncodeURIComponent(e).replace(/%3F/g, "?");
      }
    }]), r;
  }();

  r.Route, r.Context;
  var i = new r();
  return {
    Router: r,
    Routing: i
  };
});

/***/ })

},[["./assets/js/article.js","runtime","vendors~article~back_app~front_app","vendors~article"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYXJ0aWNsZS5qcyIsIndlYnBhY2s6Ly8vLi92ZW5kb3IvZnJpZW5kc29mc3ltZm9ueS9qc3JvdXRpbmctYnVuZGxlL1Jlc291cmNlcy9wdWJsaWMvanMvcm91dGVyLm1pbi5qcyJdLCJuYW1lcyI6WyIkIiwicmVxdWlyZSIsInJvdXRlcyIsIlJvdXRpbmciLCJzZXRSb3V0aW5nRGF0YSIsImNvbnNvbGUiLCJsb2ciLCJnZW5lcmF0ZSIsImF4aW9zIiwiJGFydGljbGVDb250YWluZXIiLCJzaG93QXJ0aWNsZXMiLCJkYXRhIiwiZm9yRWFjaCIsImFydGljbGUiLCJuZXdDb250ZW50IiwidXJsIiwidGl0bGUiLCJjb250ZW50IiwiYXBwZW5kIiwiZG93bmxvYWRBcnRpY2xlcyIsImFqYXgiLCJ0eXBlIiwiZGF0YVR5cGUiLCJzdWNjZXNzIiwicmVzcCIsImVycm9yIiwiZG9jdW1lbnQiLCJyZWFkeSIsImUiLCJ0IiwibiIsImRlZmluZSIsIlR5cGVFcnJvciIsIk9iamVjdCIsImFzc2lnbiIsImFyZ3VtZW50cyIsImxlbmd0aCIsIm8iLCJwcm90b3R5cGUiLCJoYXNPd25Qcm9wZXJ0eSIsImNhbGwiLCJTeW1ib2wiLCJpdGVyYXRvciIsImNvbnN0cnVjdG9yIiwiZW51bWVyYWJsZSIsImNvbmZpZ3VyYWJsZSIsIndyaXRhYmxlIiwiZGVmaW5lUHJvcGVydHkiLCJrZXkiLCJyIiwiY29udGV4dF8iLCJiYXNlX3VybCIsInByZWZpeCIsImhvc3QiLCJwb3J0Iiwic2NoZW1lIiwibG9jYWxlIiwic2V0Um91dGVzIiwidmFsdWUiLCJzZXRCYXNlVXJsIiwic2V0UHJlZml4Iiwic2V0UG9ydCIsInNldExvY2FsZSIsInNldEhvc3QiLCJzZXRTY2hlbWUiLCJyb3V0ZXNfIiwiZnJlZXplIiwiaSIsInUiLCJSZWdFeHAiLCJBcnJheSIsInRlc3QiLCJidWlsZFF1ZXJ5UGFyYW1zIiwiRXJyb3IiLCJnZXRSb3V0ZSIsInMiLCJjIiwiYSIsImwiLCJmIiwiZ2V0UG9ydCIsInRva2VucyIsImVuY29kZVBhdGhDb21wb25lbnQiLCJkZWZhdWx0cyIsImhvc3R0b2tlbnMiLCJyZXF1aXJlbWVudHMiLCJnZXRTY2hlbWUiLCJfc2NoZW1lIiwiaCIsImdldEhvc3QiLCJpbmRleE9mIiwic2NoZW1lcyIsInAiLCJrZXlzIiwiZCIsInkiLCJ2IiwicHVzaCIsImVuY29kZVF1ZXJ5Q29tcG9uZW50Iiwiam9pbiIsImdldEluc3RhbmNlIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwicmVwbGFjZSIsImN1c3RvbUVuY29kZVVSSUNvbXBvbmVudCIsIlJvdXRlIiwiQ29udGV4dCIsIlJvdXRlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxDQUFDLEdBQUdDLG1CQUFPLENBQUMsb0RBQUQsQ0FBakI7O0FBQ0EsSUFBTUMsTUFBTSxHQUFHRCxtQkFBTyxDQUFDLHFFQUFELENBQXRCOztBQUNBO0FBQ0FFLGtIQUFPLENBQUNDLGNBQVIsQ0FBdUJGLE1BQXZCO0FBQ0FHLE9BQU8sQ0FBQ0MsR0FBUixDQUFZSCxrSEFBTyxDQUFDSSxRQUFSLENBQWlCLHFCQUFqQixDQUFaOztBQUVBLElBQU1DLEtBQUssR0FBR1AsbUJBQU8sQ0FBQyw0Q0FBRCxDQUFyQjs7QUFDQSxJQUFNUSxpQkFBaUIsR0FBR1QsQ0FBQyxDQUFDLG9CQUFELENBQTNCOztBQUNBLFNBQVNVLFlBQVQsQ0FBc0JDLElBQXRCLEVBQTRCO0FBQ3hCQSxNQUFJLENBQUNDLE9BQUwsQ0FBYSxVQUFBQyxPQUFPLEVBQUk7QUFDcEIsUUFBSUMsVUFBVSwyQkFBbUJELE9BQU8sQ0FBQ0UsR0FBM0IsZ0JBQW1DRixPQUFPLENBQUNHLEtBQTNDLHlCQUErREgsT0FBTyxDQUFDSSxPQUF2RSxTQUFkO0FBQ0FSLHFCQUFpQixDQUFDUyxNQUFsQixDQUF5QkosVUFBekI7QUFDSCxHQUhEO0FBSUg7O0FBRUQsU0FBU0ssZ0JBQVQsR0FBNEI7QUFDeEJuQixHQUFDLENBQUNvQixJQUFGLENBQU87QUFDSEMsUUFBSSxFQUFFLEtBREg7QUFFSE4sT0FBRyxFQUFFWixrSEFBTyxDQUFDSSxRQUFSLENBQWlCLGtCQUFqQixDQUZGO0FBR0hlLFlBQVEsRUFBRSxNQUhQO0FBSUhDLFdBQU8sRUFBRSxpQkFBU0MsSUFBVCxFQUFjO0FBQ25CZCxrQkFBWSxDQUFDYyxJQUFELENBQVo7QUFDSCxLQU5FO0FBT0hDLFNBQUssRUFBRSxpQkFBWTtBQUNmcEIsYUFBTyxDQUFDQyxHQUFSLENBQVksT0FBWjtBQUNIO0FBVEUsR0FBUDtBQVdIOztBQUVETixDQUFDLENBQUMwQixRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzNCOzs7QUFHQVIsa0JBQWdCO0FBQ2xCLENBTEQsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QkEsQ0FBQyxVQUFTUyxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLE1BQUlDLENBQUMsR0FBQ0QsQ0FBQyxFQUFQO0FBQVUsVUFBc0NFLGlDQUFPLEVBQUQsb0NBQUlELENBQUMsQ0FBQzNCLE9BQU47QUFBQTtBQUFBO0FBQUEsb0dBQTVDLEdBQTJELFNBQTNEO0FBQTBLLENBQWxNLENBQW1NLElBQW5NLEVBQXdNLFlBQVU7QUFBQzs7QUFBYSxXQUFTeUIsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFFBQUcsRUFBRUQsQ0FBQyxZQUFZQyxDQUFmLENBQUgsRUFBcUIsTUFBTSxJQUFJRyxTQUFKLENBQWMsbUNBQWQsQ0FBTjtBQUF5RDs7QUFBQSxNQUFJSCxDQUFDLEdBQUNJLE1BQU0sQ0FBQ0MsTUFBUCxJQUFlLFVBQVNOLENBQVQsRUFBVztBQUFDLFNBQUksSUFBSUMsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDTSxTQUFTLENBQUNDLE1BQXhCLEVBQStCUCxDQUFDLEVBQWhDLEVBQW1DO0FBQUMsVUFBSUMsQ0FBQyxHQUFDSyxTQUFTLENBQUNOLENBQUQsQ0FBZjs7QUFBbUIsV0FBSSxJQUFJUSxDQUFSLElBQWFQLENBQWI7QUFBZUcsY0FBTSxDQUFDSyxTQUFQLENBQWlCQyxjQUFqQixDQUFnQ0MsSUFBaEMsQ0FBcUNWLENBQXJDLEVBQXVDTyxDQUF2QyxNQUE0Q1QsQ0FBQyxDQUFDUyxDQUFELENBQUQsR0FBS1AsQ0FBQyxDQUFDTyxDQUFELENBQWxEO0FBQWY7QUFBc0U7O0FBQUEsV0FBT1QsQ0FBUDtBQUFTLEdBQXZLO0FBQUEsTUFBd0tFLENBQUMsR0FBQyxjQUFZLE9BQU9XLE1BQW5CLElBQTJCLG9CQUFpQkEsTUFBTSxDQUFDQyxRQUF4QixDQUEzQixHQUE0RCxVQUFTZCxDQUFULEVBQVc7QUFBQyxtQkFBY0EsQ0FBZDtBQUFnQixHQUF4RixHQUF5RixVQUFTQSxDQUFULEVBQVc7QUFBQyxXQUFPQSxDQUFDLElBQUUsY0FBWSxPQUFPYSxNQUF0QixJQUE4QmIsQ0FBQyxDQUFDZSxXQUFGLEtBQWdCRixNQUE5QyxJQUFzRGIsQ0FBQyxLQUFHYSxNQUFNLENBQUNILFNBQWpFLEdBQTJFLFFBQTNFLFdBQTJGVixDQUEzRixDQUFQO0FBQW9HLEdBQW5YO0FBQUEsTUFBb1hTLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBU1QsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFdBQUksSUFBSUMsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDRCxDQUFDLENBQUNPLE1BQWhCLEVBQXVCTixDQUFDLEVBQXhCLEVBQTJCO0FBQUMsWUFBSU8sQ0FBQyxHQUFDUixDQUFDLENBQUNDLENBQUQsQ0FBUDtBQUFXTyxTQUFDLENBQUNPLFVBQUYsR0FBYVAsQ0FBQyxDQUFDTyxVQUFGLElBQWMsQ0FBQyxDQUE1QixFQUE4QlAsQ0FBQyxDQUFDUSxZQUFGLEdBQWUsQ0FBQyxDQUE5QyxFQUFnRCxXQUFVUixDQUFWLEtBQWNBLENBQUMsQ0FBQ1MsUUFBRixHQUFXLENBQUMsQ0FBMUIsQ0FBaEQsRUFBNkViLE1BQU0sQ0FBQ2MsY0FBUCxDQUFzQm5CLENBQXRCLEVBQXdCUyxDQUFDLENBQUNXLEdBQTFCLEVBQThCWCxDQUE5QixDQUE3RTtBQUE4RztBQUFDOztBQUFBLFdBQU8sVUFBU1IsQ0FBVCxFQUFXQyxDQUFYLEVBQWFPLENBQWIsRUFBZTtBQUFDLGFBQU9QLENBQUMsSUFBRUYsQ0FBQyxDQUFDQyxDQUFDLENBQUNTLFNBQUgsRUFBYVIsQ0FBYixDQUFKLEVBQW9CTyxDQUFDLElBQUVULENBQUMsQ0FBQ0MsQ0FBRCxFQUFHUSxDQUFILENBQXhCLEVBQThCUixDQUFyQztBQUF1QyxLQUE5RDtBQUErRCxHQUFoUCxFQUF0WDtBQUFBLE1BQXltQm9CLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBU0EsQ0FBVCxDQUFXcEIsQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQ0YsT0FBQyxDQUFDLElBQUQsRUFBTXFCLENBQU4sQ0FBRCxFQUFVLEtBQUtDLFFBQUwsR0FBY3JCLENBQUMsSUFBRTtBQUFDc0IsZ0JBQVEsRUFBQyxFQUFWO0FBQWFDLGNBQU0sRUFBQyxFQUFwQjtBQUF1QkMsWUFBSSxFQUFDLEVBQTVCO0FBQStCQyxZQUFJLEVBQUMsRUFBcEM7QUFBdUNDLGNBQU0sRUFBQyxFQUE5QztBQUFpREMsY0FBTSxFQUFDO0FBQXhELE9BQTNCLEVBQXVGLEtBQUtDLFNBQUwsQ0FBZTNCLENBQUMsSUFBRSxFQUFsQixDQUF2RjtBQUE2Rzs7QUFBQSxXQUFPTyxDQUFDLENBQUNZLENBQUQsRUFBRyxDQUFDO0FBQUNELFNBQUcsRUFBQyxnQkFBTDtBQUFzQlUsV0FBSyxFQUFDLGVBQVM5QixDQUFULEVBQVc7QUFBQyxhQUFLK0IsVUFBTCxDQUFnQi9CLENBQUMsQ0FBQ3VCLFFBQWxCLEdBQTRCLEtBQUtNLFNBQUwsQ0FBZTdCLENBQUMsQ0FBQzFCLE1BQWpCLENBQTVCLEVBQXFELFlBQVcwQixDQUFYLElBQWMsS0FBS2dDLFNBQUwsQ0FBZWhDLENBQUMsQ0FBQ3dCLE1BQWpCLENBQW5FLEVBQTRGLFVBQVN4QixDQUFULElBQVksS0FBS2lDLE9BQUwsQ0FBYWpDLENBQUMsQ0FBQzBCLElBQWYsQ0FBeEcsRUFBNkgsWUFBVzFCLENBQVgsSUFBYyxLQUFLa0MsU0FBTCxDQUFlbEMsQ0FBQyxDQUFDNEIsTUFBakIsQ0FBM0ksRUFBb0ssS0FBS08sT0FBTCxDQUFhbkMsQ0FBQyxDQUFDeUIsSUFBZixDQUFwSyxFQUF5TCxLQUFLVyxTQUFMLENBQWVwQyxDQUFDLENBQUMyQixNQUFqQixDQUF6TDtBQUFrTjtBQUExUCxLQUFELEVBQTZQO0FBQUNQLFNBQUcsRUFBQyxXQUFMO0FBQWlCVSxXQUFLLEVBQUMsZUFBUzlCLENBQVQsRUFBVztBQUFDLGFBQUtxQyxPQUFMLEdBQWFoQyxNQUFNLENBQUNpQyxNQUFQLENBQWN0QyxDQUFkLENBQWI7QUFBOEI7QUFBakUsS0FBN1AsRUFBZ1U7QUFBQ29CLFNBQUcsRUFBQyxXQUFMO0FBQWlCVSxXQUFLLEVBQUMsaUJBQVU7QUFBQyxlQUFPLEtBQUtPLE9BQVo7QUFBb0I7QUFBdEQsS0FBaFUsRUFBd1g7QUFBQ2pCLFNBQUcsRUFBQyxZQUFMO0FBQWtCVSxXQUFLLEVBQUMsZUFBUzlCLENBQVQsRUFBVztBQUFDLGFBQUtzQixRQUFMLENBQWNDLFFBQWQsR0FBdUJ2QixDQUF2QjtBQUF5QjtBQUE3RCxLQUF4WCxFQUF1YjtBQUFDb0IsU0FBRyxFQUFDLFlBQUw7QUFBa0JVLFdBQUssRUFBQyxpQkFBVTtBQUFDLGVBQU8sS0FBS1IsUUFBTCxDQUFjQyxRQUFyQjtBQUE4QjtBQUFqRSxLQUF2YixFQUEwZjtBQUFDSCxTQUFHLEVBQUMsV0FBTDtBQUFpQlUsV0FBSyxFQUFDLGVBQVM5QixDQUFULEVBQVc7QUFBQyxhQUFLc0IsUUFBTCxDQUFjRSxNQUFkLEdBQXFCeEIsQ0FBckI7QUFBdUI7QUFBMUQsS0FBMWYsRUFBc2pCO0FBQUNvQixTQUFHLEVBQUMsV0FBTDtBQUFpQlUsV0FBSyxFQUFDLGVBQVM5QixDQUFULEVBQVc7QUFBQyxhQUFLc0IsUUFBTCxDQUFjSyxNQUFkLEdBQXFCM0IsQ0FBckI7QUFBdUI7QUFBMUQsS0FBdGpCLEVBQWtuQjtBQUFDb0IsU0FBRyxFQUFDLFdBQUw7QUFBaUJVLFdBQUssRUFBQyxpQkFBVTtBQUFDLGVBQU8sS0FBS1IsUUFBTCxDQUFjSyxNQUFyQjtBQUE0QjtBQUE5RCxLQUFsbkIsRUFBa3JCO0FBQUNQLFNBQUcsRUFBQyxTQUFMO0FBQWVVLFdBQUssRUFBQyxlQUFTOUIsQ0FBVCxFQUFXO0FBQUMsYUFBS3NCLFFBQUwsQ0FBY0csSUFBZCxHQUFtQnpCLENBQW5CO0FBQXFCO0FBQXRELEtBQWxyQixFQUEwdUI7QUFBQ29CLFNBQUcsRUFBQyxTQUFMO0FBQWVVLFdBQUssRUFBQyxpQkFBVTtBQUFDLGVBQU8sS0FBS1IsUUFBTCxDQUFjRyxJQUFyQjtBQUEwQjtBQUExRCxLQUExdUIsRUFBc3lCO0FBQUNMLFNBQUcsRUFBQyxTQUFMO0FBQWVVLFdBQUssRUFBQyxlQUFTOUIsQ0FBVCxFQUFXO0FBQUMsYUFBS3NCLFFBQUwsQ0FBY0ksSUFBZCxHQUFtQjFCLENBQW5CO0FBQXFCO0FBQXRELEtBQXR5QixFQUE4MUI7QUFBQ29CLFNBQUcsRUFBQyxTQUFMO0FBQWVVLFdBQUssRUFBQyxpQkFBVTtBQUFDLGVBQU8sS0FBS1IsUUFBTCxDQUFjSSxJQUFyQjtBQUEwQjtBQUExRCxLQUE5MUIsRUFBMDVCO0FBQUNOLFNBQUcsRUFBQyxXQUFMO0FBQWlCVSxXQUFLLEVBQUMsZUFBUzlCLENBQVQsRUFBVztBQUFDLGFBQUtzQixRQUFMLENBQWNNLE1BQWQsR0FBcUI1QixDQUFyQjtBQUF1QjtBQUExRCxLQUExNUIsRUFBczlCO0FBQUNvQixTQUFHLEVBQUMsV0FBTDtBQUFpQlUsV0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBTyxLQUFLUixRQUFMLENBQWNNLE1BQXJCO0FBQTRCO0FBQTlELEtBQXQ5QixFQUFzaEM7QUFBQ1IsU0FBRyxFQUFDLGtCQUFMO0FBQXdCVSxXQUFLLEVBQUMsZUFBUzlCLENBQVQsRUFBV0MsQ0FBWCxFQUFhUSxDQUFiLEVBQWU7QUFBQyxZQUFJWSxDQUFDLEdBQUMsSUFBTjtBQUFBLFlBQVdrQixDQUFDLEdBQUMsS0FBSyxDQUFsQjtBQUFBLFlBQW9CQyxDQUFDLEdBQUMsSUFBSUMsTUFBSixDQUFXLE9BQVgsQ0FBdEI7QUFBMEMsWUFBR3hDLENBQUMsWUFBWXlDLEtBQWhCLEVBQXNCekMsQ0FBQyxDQUFDakIsT0FBRixDQUFVLFVBQVNpQixDQUFULEVBQVdzQyxDQUFYLEVBQWE7QUFBQ0MsV0FBQyxDQUFDRyxJQUFGLENBQU8zQyxDQUFQLElBQVVTLENBQUMsQ0FBQ1QsQ0FBRCxFQUFHQyxDQUFILENBQVgsR0FBaUJvQixDQUFDLENBQUN1QixnQkFBRixDQUFtQjVDLENBQUMsR0FBQyxHQUFGLElBQU8sY0FBWSxlQUFhLE9BQU9DLENBQXBCLEdBQXNCLFdBQXRCLEdBQWtDQyxDQUFDLENBQUNELENBQUQsQ0FBL0MsSUFBb0RzQyxDQUFwRCxHQUFzRCxFQUE3RCxJQUFpRSxHQUFwRixFQUF3RnRDLENBQXhGLEVBQTBGUSxDQUExRixDQUFqQjtBQUE4RyxTQUF0SSxFQUF0QixLQUFtSyxJQUFHLGNBQVksZUFBYSxPQUFPUixDQUFwQixHQUFzQixXQUF0QixHQUFrQ0MsQ0FBQyxDQUFDRCxDQUFELENBQS9DLENBQUgsRUFBdUQsS0FBSXNDLENBQUosSUFBU3RDLENBQVQ7QUFBVyxlQUFLMkMsZ0JBQUwsQ0FBc0I1QyxDQUFDLEdBQUMsR0FBRixHQUFNdUMsQ0FBTixHQUFRLEdBQTlCLEVBQWtDdEMsQ0FBQyxDQUFDc0MsQ0FBRCxDQUFuQyxFQUF1QzlCLENBQXZDO0FBQVgsU0FBdkQsTUFBaUhBLENBQUMsQ0FBQ1QsQ0FBRCxFQUFHQyxDQUFILENBQUQ7QUFBTztBQUFuWCxLQUF0aEMsRUFBMjRDO0FBQUNtQixTQUFHLEVBQUMsVUFBTDtBQUFnQlUsV0FBSyxFQUFDLGVBQVM5QixDQUFULEVBQVc7QUFBQyxZQUFJQyxDQUFDLEdBQUMsS0FBS3FCLFFBQUwsQ0FBY0UsTUFBZCxHQUFxQnhCLENBQTNCO0FBQUEsWUFBNkJFLENBQUMsR0FBQ0YsQ0FBQyxHQUFDLEdBQUYsR0FBTSxLQUFLc0IsUUFBTCxDQUFjTSxNQUFuRDtBQUFBLFlBQTBEbkIsQ0FBQyxHQUFDLEtBQUthLFFBQUwsQ0FBY0UsTUFBZCxHQUFxQnhCLENBQXJCLEdBQXVCLEdBQXZCLEdBQTJCLEtBQUtzQixRQUFMLENBQWNNLE1BQXJHO0FBQUEsWUFBNEdQLENBQUMsR0FBQyxDQUFDcEIsQ0FBRCxFQUFHQyxDQUFILEVBQUtPLENBQUwsRUFBT1QsQ0FBUCxDQUE5Rzs7QUFBd0gsYUFBSSxJQUFJdUMsQ0FBUixJQUFhbEIsQ0FBYjtBQUFlLGNBQUdBLENBQUMsQ0FBQ2tCLENBQUQsQ0FBRCxJQUFPLEtBQUtGLE9BQWYsRUFBdUIsT0FBTyxLQUFLQSxPQUFMLENBQWFoQixDQUFDLENBQUNrQixDQUFELENBQWQsQ0FBUDtBQUF0Qzs7QUFBZ0UsY0FBTSxJQUFJTSxLQUFKLENBQVUsZ0JBQWM3QyxDQUFkLEdBQWdCLG1CQUExQixDQUFOO0FBQXFEO0FBQS9RLEtBQTM0QyxFQUE0cEQ7QUFBQ29CLFNBQUcsRUFBQyxVQUFMO0FBQWdCVSxXQUFLLEVBQUMsZUFBUzlCLENBQVQsRUFBV0UsQ0FBWCxFQUFhO0FBQUMsWUFBSU8sQ0FBQyxHQUFDRixTQUFTLENBQUNDLE1BQVYsR0FBaUIsQ0FBakIsSUFBb0IsS0FBSyxDQUFMLEtBQVNELFNBQVMsQ0FBQyxDQUFELENBQXRDLElBQTJDQSxTQUFTLENBQUMsQ0FBRCxDQUExRDtBQUFBLFlBQThEZ0MsQ0FBQyxHQUFDLEtBQUtPLFFBQUwsQ0FBYzlDLENBQWQsQ0FBaEU7QUFBQSxZQUFpRndDLENBQUMsR0FBQ3RDLENBQUMsSUFBRSxFQUF0RjtBQUFBLFlBQXlGNkMsQ0FBQyxHQUFDOUMsQ0FBQyxDQUFDLEVBQUQsRUFBSXVDLENBQUosQ0FBNUY7QUFBQSxZQUFtR1EsQ0FBQyxHQUFDLEVBQXJHO0FBQUEsWUFBd0dDLENBQUMsR0FBQyxDQUFDLENBQTNHO0FBQUEsWUFBNkdDLENBQUMsR0FBQyxFQUEvRztBQUFBLFlBQWtIQyxDQUFDLEdBQUMsZUFBYSxPQUFPLEtBQUtDLE9BQUwsRUFBcEIsSUFBb0MsU0FBTyxLQUFLQSxPQUFMLEVBQTNDLEdBQTBELEVBQTFELEdBQTZELEtBQUtBLE9BQUwsRUFBakw7O0FBQWdNLFlBQUdiLENBQUMsQ0FBQ2MsTUFBRixDQUFTckUsT0FBVCxDQUFpQixVQUFTaUIsQ0FBVCxFQUFXO0FBQUMsY0FBRyxXQUFTQSxDQUFDLENBQUMsQ0FBRCxDQUFiLEVBQWlCLE9BQU8rQyxDQUFDLEdBQUMzQixDQUFDLENBQUNpQyxtQkFBRixDQUFzQnJELENBQUMsQ0FBQyxDQUFELENBQXZCLElBQTRCK0MsQ0FBOUIsRUFBZ0MsTUFBS0MsQ0FBQyxHQUFDLENBQUMsQ0FBUixDQUF2QztBQUFrRDtBQUFDLGdCQUFHLGVBQWFoRCxDQUFDLENBQUMsQ0FBRCxDQUFqQixFQUFxQixNQUFNLElBQUk0QyxLQUFKLENBQVUscUJBQW1CNUMsQ0FBQyxDQUFDLENBQUQsQ0FBcEIsR0FBd0IscUJBQWxDLENBQU47QUFBK0QsZ0JBQUlDLENBQUMsR0FBQ3FDLENBQUMsQ0FBQ2dCLFFBQUYsSUFBWXRELENBQUMsQ0FBQyxDQUFELENBQUQsSUFBT3NDLENBQUMsQ0FBQ2dCLFFBQTNCOztBQUFvQyxnQkFBRyxDQUFDLENBQUQsS0FBS04sQ0FBTCxJQUFRLENBQUMvQyxDQUFULElBQVlELENBQUMsQ0FBQyxDQUFELENBQUQsSUFBT3VDLENBQVAsSUFBVUEsQ0FBQyxDQUFDdkMsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFELElBQVNzQyxDQUFDLENBQUNnQixRQUFGLENBQVd0RCxDQUFDLENBQUMsQ0FBRCxDQUFaLENBQWxDLEVBQW1EO0FBQUMsa0JBQUlRLENBQUMsR0FBQyxLQUFLLENBQVg7QUFBYSxrQkFBR1IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFPdUMsQ0FBVixFQUFZL0IsQ0FBQyxHQUFDK0IsQ0FBQyxDQUFDdkMsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFILEVBQVUsT0FBTzhDLENBQUMsQ0FBQzlDLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBbEIsQ0FBWixLQUF5QztBQUFDLG9CQUFHLENBQUNDLENBQUosRUFBTTtBQUFDLHNCQUFHK0MsQ0FBSCxFQUFLO0FBQU8sd0JBQU0sSUFBSUosS0FBSixDQUFVLGdCQUFjN0MsQ0FBZCxHQUFnQiw0QkFBaEIsR0FBNkNDLENBQUMsQ0FBQyxDQUFELENBQTlDLEdBQWtELElBQTVELENBQU47QUFBd0U7O0FBQUFRLGlCQUFDLEdBQUM4QixDQUFDLENBQUNnQixRQUFGLENBQVd0RCxDQUFDLENBQUMsQ0FBRCxDQUFaLENBQUY7QUFBbUI7QUFBQSxrQkFBSWlELENBQUMsR0FBQyxDQUFDLENBQUQsS0FBS3pDLENBQUwsSUFBUSxDQUFDLENBQUQsS0FBS0EsQ0FBYixJQUFnQixPQUFLQSxDQUEzQjs7QUFBNkIsa0JBQUcsQ0FBQ3lDLENBQUQsSUFBSSxDQUFDRCxDQUFSLEVBQVU7QUFBQyxvQkFBSUUsQ0FBQyxHQUFDOUIsQ0FBQyxDQUFDaUMsbUJBQUYsQ0FBc0I3QyxDQUF0QixDQUFOO0FBQStCLDJCQUFTMEMsQ0FBVCxJQUFZLFNBQU8xQyxDQUFuQixLQUF1QjBDLENBQUMsR0FBQyxFQUF6QixHQUE2QkgsQ0FBQyxHQUFDL0MsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLa0QsQ0FBTCxHQUFPSCxDQUF0QztBQUF3Qzs7QUFBQUMsZUFBQyxHQUFDLENBQUMsQ0FBSDtBQUFLLGFBQTdVLE1BQWtWL0MsQ0FBQyxJQUFFRCxDQUFDLENBQUMsQ0FBRCxDQUFELElBQU84QyxDQUFWLElBQWEsT0FBT0EsQ0FBQyxDQUFDOUMsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFyQjtBQUE0QjtBQUFDLFNBQXhrQixHQUEwa0IsT0FBSytDLENBQUwsS0FBU0EsQ0FBQyxHQUFDLEdBQVgsQ0FBMWtCLEVBQTBsQlQsQ0FBQyxDQUFDaUIsVUFBRixDQUFheEUsT0FBYixDQUFxQixVQUFTZ0IsQ0FBVCxFQUFXO0FBQUMsY0FBSUMsQ0FBQyxHQUFDLEtBQUssQ0FBWDtBQUFhLGlCQUFNLFdBQVNELENBQUMsQ0FBQyxDQUFELENBQVYsR0FBYyxNQUFLa0QsQ0FBQyxHQUFDbEQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLa0QsQ0FBWixDQUFkLEdBQTZCLE1BQUssZUFBYWxELENBQUMsQ0FBQyxDQUFELENBQWQsS0FBb0JBLENBQUMsQ0FBQyxDQUFELENBQUQsSUFBT3dDLENBQVAsSUFBVXZDLENBQUMsR0FBQ3VDLENBQUMsQ0FBQ3hDLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBSCxFQUFVLE9BQU8rQyxDQUFDLENBQUMvQyxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQTVCLElBQW9DdUMsQ0FBQyxDQUFDZ0IsUUFBRixJQUFZdkQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFPdUMsQ0FBQyxDQUFDZ0IsUUFBckIsS0FBZ0N0RCxDQUFDLEdBQUNzQyxDQUFDLENBQUNnQixRQUFGLENBQVd2RCxDQUFDLENBQUMsQ0FBRCxDQUFaLENBQWxDLENBQXBDLEVBQXdGa0QsQ0FBQyxHQUFDbEQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQyxDQUFMLEdBQU9pRCxDQUFySCxDQUFMLENBQW5DO0FBQWlLLFNBQS9NLENBQTFsQixFQUEyeUJGLENBQUMsR0FBQyxLQUFLMUIsUUFBTCxDQUFjQyxRQUFkLEdBQXVCeUIsQ0FBcDBCLEVBQXMwQlQsQ0FBQyxDQUFDa0IsWUFBRixJQUFnQixhQUFZbEIsQ0FBQyxDQUFDa0IsWUFBOUIsSUFBNEMsS0FBS0MsU0FBTCxNQUFrQm5CLENBQUMsQ0FBQ2tCLFlBQUYsQ0FBZUUsT0FBdDVCLEVBQTg1QjtBQUFDLGNBQUlDLENBQUMsR0FBQ1YsQ0FBQyxJQUFFLEtBQUtXLE9BQUwsRUFBVDtBQUF3QmIsV0FBQyxHQUFDVCxDQUFDLENBQUNrQixZQUFGLENBQWVFLE9BQWYsR0FBdUIsS0FBdkIsR0FBNkJDLENBQTdCLElBQWdDQSxDQUFDLENBQUNFLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBbkUsSUFBc0VILENBQXhFO0FBQTBFLFNBQWpnQyxNQUFzZ0MsSUFBRyxlQUFhLE9BQU9ULENBQUMsQ0FBQ3dCLE9BQXRCLElBQStCLGVBQWEsT0FBT3hCLENBQUMsQ0FBQ3dCLE9BQUYsQ0FBVSxDQUFWLENBQW5ELElBQWlFLEtBQUtMLFNBQUwsT0FBbUJuQixDQUFDLENBQUN3QixPQUFGLENBQVUsQ0FBVixDQUF2RixFQUFvRztBQUFDLGNBQUlDLENBQUMsR0FBQ2QsQ0FBQyxJQUFFLEtBQUtXLE9BQUwsRUFBVDtBQUF3QmIsV0FBQyxHQUFDVCxDQUFDLENBQUN3QixPQUFGLENBQVUsQ0FBVixJQUFhLEtBQWIsR0FBbUJDLENBQW5CLElBQXNCQSxDQUFDLENBQUNGLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBekQsSUFBNERILENBQTlEO0FBQWdFLFNBQTdMLE1BQWtNRSxDQUFDLElBQUUsS0FBS1csT0FBTCxPQUFpQlgsQ0FBQyxJQUFFQSxDQUFDLENBQUNZLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBckMsQ0FBckIsR0FBNkRILENBQUMsR0FBQyxLQUFLVSxTQUFMLEtBQWlCLEtBQWpCLEdBQXVCUixDQUF2QixJQUEwQkEsQ0FBQyxDQUFDWSxPQUFGLENBQVUsTUFBSVgsQ0FBZCxJQUFpQixDQUFDLENBQWxCLElBQXFCLE9BQUtBLENBQTFCLEdBQTRCLEVBQTVCLEdBQStCLE1BQUlBLENBQTdELElBQWdFSCxDQUEvSCxHQUFpSXZDLENBQUMsS0FBRyxDQUFDLENBQUwsS0FBU3VDLENBQUMsR0FBQyxLQUFLVSxTQUFMLEtBQWlCLEtBQWpCLEdBQXVCLEtBQUtHLE9BQUwsRUFBdkIsSUFBdUMsS0FBS0EsT0FBTCxHQUFlQyxPQUFmLENBQXVCLE1BQUlYLENBQTNCLElBQThCLENBQUMsQ0FBL0IsSUFBa0MsT0FBS0EsQ0FBdkMsR0FBeUMsRUFBekMsR0FBNEMsTUFBSUEsQ0FBdkYsSUFBMEZILENBQXJHLENBQWpJOztBQUF5TyxZQUFHM0MsTUFBTSxDQUFDNEQsSUFBUCxDQUFZbEIsQ0FBWixFQUFldkMsTUFBZixHQUFzQixDQUF6QixFQUEyQjtBQUFDLGNBQUkwRCxDQUFDLEdBQUMsS0FBSyxDQUFYO0FBQUEsY0FBYUMsQ0FBQyxHQUFDLEVBQWY7QUFBQSxjQUFrQkMsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBU3BFLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUNBLGFBQUMsR0FBQyxjQUFZLE9BQU9BLENBQW5CLEdBQXFCQSxDQUFDLEVBQXRCLEdBQXlCQSxDQUEzQixFQUE2QkEsQ0FBQyxHQUFDLFNBQU9BLENBQVAsR0FBUyxFQUFULEdBQVlBLENBQTNDLEVBQTZDa0UsQ0FBQyxDQUFDRSxJQUFGLENBQU9oRCxDQUFDLENBQUNpRCxvQkFBRixDQUF1QnRFLENBQXZCLElBQTBCLEdBQTFCLEdBQThCcUIsQ0FBQyxDQUFDaUQsb0JBQUYsQ0FBdUJyRSxDQUF2QixDQUFyQyxDQUE3QztBQUE2RyxXQUEvSTs7QUFBZ0osZUFBSWlFLENBQUosSUFBU25CLENBQVQ7QUFBVyxpQkFBS0gsZ0JBQUwsQ0FBc0JzQixDQUF0QixFQUF3Qm5CLENBQUMsQ0FBQ21CLENBQUQsQ0FBekIsRUFBNkJFLENBQTdCO0FBQVg7O0FBQTJDcEIsV0FBQyxHQUFDQSxDQUFDLEdBQUMsR0FBRixHQUFNbUIsQ0FBQyxDQUFDSSxJQUFGLENBQU8sR0FBUCxDQUFSO0FBQW9COztBQUFBLGVBQU92QixDQUFQO0FBQVM7QUFBejRELEtBQTVwRCxDQUFILEVBQTJpSCxDQUFDO0FBQUM1QixTQUFHLEVBQUMsYUFBTDtBQUFtQlUsV0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBT1MsQ0FBUDtBQUFTO0FBQTdDLEtBQUQsRUFBZ0Q7QUFBQ25CLFNBQUcsRUFBQyxTQUFMO0FBQWVVLFdBQUssRUFBQyxlQUFTOUIsQ0FBVCxFQUFXO0FBQUMsWUFBSUMsQ0FBQyxHQUFDb0IsQ0FBQyxDQUFDbUQsV0FBRixFQUFOO0FBQXNCdkUsU0FBQyxDQUFDekIsY0FBRixDQUFpQndCLENBQWpCO0FBQW9CO0FBQTNFLEtBQWhELEVBQTZIO0FBQUNvQixTQUFHLEVBQUMsMEJBQUw7QUFBZ0NVLFdBQUssRUFBQyxlQUFTOUIsQ0FBVCxFQUFXO0FBQUMsZUFBT3lFLGtCQUFrQixDQUFDekUsQ0FBRCxDQUFsQixDQUFzQjBFLE9BQXRCLENBQThCLE1BQTlCLEVBQXFDLEdBQXJDLEVBQTBDQSxPQUExQyxDQUFrRCxNQUFsRCxFQUF5RCxHQUF6RCxFQUE4REEsT0FBOUQsQ0FBc0UsTUFBdEUsRUFBNkUsR0FBN0UsRUFBa0ZBLE9BQWxGLENBQTBGLE1BQTFGLEVBQWlHLEdBQWpHLEVBQXNHQSxPQUF0RyxDQUE4RyxNQUE5RyxFQUFxSCxHQUFySCxFQUEwSEEsT0FBMUgsQ0FBa0ksTUFBbEksRUFBeUksR0FBekksRUFBOElBLE9BQTlJLENBQXNKLE1BQXRKLEVBQTZKLEdBQTdKLEVBQWtLQSxPQUFsSyxDQUEwSyxLQUExSyxFQUFnTCxLQUFoTCxFQUF1TEEsT0FBdkwsQ0FBK0wsS0FBL0wsRUFBcU0sS0FBck0sRUFBNE1BLE9BQTVNLENBQW9OLElBQXBOLEVBQXlOLEtBQXpOLENBQVA7QUFBdU87QUFBelIsS0FBN0gsRUFBd1o7QUFBQ3RELFNBQUcsRUFBQyxxQkFBTDtBQUEyQlUsV0FBSyxFQUFDLGVBQVM5QixDQUFULEVBQVc7QUFBQyxlQUFPcUIsQ0FBQyxDQUFDc0Qsd0JBQUYsQ0FBMkIzRSxDQUEzQixFQUE4QjBFLE9BQTlCLENBQXNDLE1BQXRDLEVBQTZDLEdBQTdDLEVBQWtEQSxPQUFsRCxDQUEwRCxNQUExRCxFQUFpRSxHQUFqRSxFQUFzRUEsT0FBdEUsQ0FBOEUsTUFBOUUsRUFBcUYsR0FBckYsRUFBMEZBLE9BQTFGLENBQWtHLE1BQWxHLEVBQXlHLEdBQXpHLENBQVA7QUFBcUg7QUFBbEssS0FBeFosRUFBNGpCO0FBQUN0RCxTQUFHLEVBQUMsc0JBQUw7QUFBNEJVLFdBQUssRUFBQyxlQUFTOUIsQ0FBVCxFQUFXO0FBQUMsZUFBT3FCLENBQUMsQ0FBQ3NELHdCQUFGLENBQTJCM0UsQ0FBM0IsRUFBOEIwRSxPQUE5QixDQUFzQyxNQUF0QyxFQUE2QyxHQUE3QyxDQUFQO0FBQXlEO0FBQXZHLEtBQTVqQixDQUEzaUgsQ0FBRCxFQUFtdElyRCxDQUExdEk7QUFBNHRJLEdBQXAySSxFQUEzbUI7O0FBQWs5SkEsR0FBQyxDQUFDdUQsS0FBRixFQUFRdkQsQ0FBQyxDQUFDd0QsT0FBVjtBQUFrQixNQUFJdEMsQ0FBQyxHQUFDLElBQUlsQixDQUFKLEVBQU47QUFBWSxTQUFNO0FBQUN5RCxVQUFNLEVBQUN6RCxDQUFSO0FBQVU5QyxXQUFPLEVBQUNnRTtBQUFsQixHQUFOO0FBQTJCLENBQXowSyxDQUFELEMiLCJmaWxlIjoiYXJ0aWNsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0ICQgPSByZXF1aXJlKCdqcXVlcnknKTtcbmNvbnN0IHJvdXRlcyA9IHJlcXVpcmUoJy4uLy4uL3B1YmxpYy9qcy9mb3NfanNfcm91dGVzJyk7XG5pbXBvcnQgUm91dGluZyBmcm9tICcuLi8uLi92ZW5kb3IvZnJpZW5kc29mc3ltZm9ueS9qc3JvdXRpbmctYnVuZGxlL1Jlc291cmNlcy9wdWJsaWMvanMvcm91dGVyLm1pbi5qcyc7XG5Sb3V0aW5nLnNldFJvdXRpbmdEYXRhKHJvdXRlcyk7XG5jb25zb2xlLmxvZyhSb3V0aW5nLmdlbmVyYXRlKCdhY3R1YWxpdGVzX2FydGljbGVzJykpO1xuXG5jb25zdCBheGlvcyA9IHJlcXVpcmUoJ2F4aW9zJyk7XG5jb25zdCAkYXJ0aWNsZUNvbnRhaW5lciA9ICQoJyNhcnRpY2xlLWNvbnRhaW5lcicpO1xuZnVuY3Rpb24gc2hvd0FydGljbGVzKGRhdGEpIHtcbiAgICBkYXRhLmZvckVhY2goYXJ0aWNsZSA9PiB7XG4gICAgICAgIGxldCBuZXdDb250ZW50ID0gYDxoMT48YSBocmVmPVwiJHthcnRpY2xlLnVybH1cIj4ke2FydGljbGUudGl0bGV9PC9hPjwvaDE+PHA+JHthcnRpY2xlLmNvbnRlbnR9PC9wPmA7XG4gICAgICAgICRhcnRpY2xlQ29udGFpbmVyLmFwcGVuZChuZXdDb250ZW50KTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gZG93bmxvYWRBcnRpY2xlcygpIHtcbiAgICAkLmFqYXgoe1xuICAgICAgICB0eXBlOiAnR0VUJyxcbiAgICAgICAgdXJsOiBSb3V0aW5nLmdlbmVyYXRlKCdhY3R1YWxpdGVzX2hlbGxvJyksXG4gICAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3Ape1xuICAgICAgICAgICAgc2hvd0FydGljbGVzKHJlc3ApO1xuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yJyk7XG4gICAgICAgIH1cbiAgICB9KVxufVxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICAvKiQoJyNidG4tc2hvdy1hcnRpY2xlJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgIGRvd25sb2FkQXJ0aWNsZXMoKTtcbiAgIH0pKi9cbiAgIGRvd25sb2FkQXJ0aWNsZXMoKTtcbn0pOyIsIiFmdW5jdGlvbihlLHQpe3ZhciBuPXQoKTtcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtdLG4uUm91dGluZyk6XCJvYmplY3RcIj09dHlwZW9mIG1vZHVsZSYmbW9kdWxlLmV4cG9ydHM/bW9kdWxlLmV4cG9ydHM9bi5Sb3V0aW5nOihlLlJvdXRpbmc9bi5Sb3V0aW5nLGUuZm9zPXtSb3V0ZXI6bi5Sb3V0ZXJ9KX0odGhpcyxmdW5jdGlvbigpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGUoZSx0KXtpZighKGUgaW5zdGFuY2VvZiB0KSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpfXZhciB0PU9iamVjdC5hc3NpZ258fGZ1bmN0aW9uKGUpe2Zvcih2YXIgdD0xO3Q8YXJndW1lbnRzLmxlbmd0aDt0Kyspe3ZhciBuPWFyZ3VtZW50c1t0XTtmb3IodmFyIG8gaW4gbilPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobixvKSYmKGVbb109bltvXSl9cmV0dXJuIGV9LG49XCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZcInN5bWJvbFwiPT10eXBlb2YgU3ltYm9sLml0ZXJhdG9yP2Z1bmN0aW9uKGUpe3JldHVybiB0eXBlb2YgZX06ZnVuY3Rpb24oZSl7cmV0dXJuIGUmJlwiZnVuY3Rpb25cIj09dHlwZW9mIFN5bWJvbCYmZS5jb25zdHJ1Y3Rvcj09PVN5bWJvbCYmZSE9PVN5bWJvbC5wcm90b3R5cGU/XCJzeW1ib2xcIjp0eXBlb2YgZX0sbz1mdW5jdGlvbigpe2Z1bmN0aW9uIGUoZSx0KXtmb3IodmFyIG49MDtuPHQubGVuZ3RoO24rKyl7dmFyIG89dFtuXTtvLmVudW1lcmFibGU9by5lbnVtZXJhYmxlfHwhMSxvLmNvbmZpZ3VyYWJsZT0hMCxcInZhbHVlXCJpbiBvJiYoby53cml0YWJsZT0hMCksT2JqZWN0LmRlZmluZVByb3BlcnR5KGUsby5rZXksbyl9fXJldHVybiBmdW5jdGlvbih0LG4sbyl7cmV0dXJuIG4mJmUodC5wcm90b3R5cGUsbiksbyYmZSh0LG8pLHR9fSgpLHI9ZnVuY3Rpb24oKXtmdW5jdGlvbiByKHQsbil7ZSh0aGlzLHIpLHRoaXMuY29udGV4dF89dHx8e2Jhc2VfdXJsOlwiXCIscHJlZml4OlwiXCIsaG9zdDpcIlwiLHBvcnQ6XCJcIixzY2hlbWU6XCJcIixsb2NhbGU6XCJcIn0sdGhpcy5zZXRSb3V0ZXMobnx8e30pfXJldHVybiBvKHIsW3trZXk6XCJzZXRSb3V0aW5nRGF0YVwiLHZhbHVlOmZ1bmN0aW9uKGUpe3RoaXMuc2V0QmFzZVVybChlLmJhc2VfdXJsKSx0aGlzLnNldFJvdXRlcyhlLnJvdXRlcyksXCJwcmVmaXhcImluIGUmJnRoaXMuc2V0UHJlZml4KGUucHJlZml4KSxcInBvcnRcImluIGUmJnRoaXMuc2V0UG9ydChlLnBvcnQpLFwibG9jYWxlXCJpbiBlJiZ0aGlzLnNldExvY2FsZShlLmxvY2FsZSksdGhpcy5zZXRIb3N0KGUuaG9zdCksdGhpcy5zZXRTY2hlbWUoZS5zY2hlbWUpfX0se2tleTpcInNldFJvdXRlc1wiLHZhbHVlOmZ1bmN0aW9uKGUpe3RoaXMucm91dGVzXz1PYmplY3QuZnJlZXplKGUpfX0se2tleTpcImdldFJvdXRlc1wiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucm91dGVzX319LHtrZXk6XCJzZXRCYXNlVXJsXCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5jb250ZXh0Xy5iYXNlX3VybD1lfX0se2tleTpcImdldEJhc2VVcmxcIix2YWx1ZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLmNvbnRleHRfLmJhc2VfdXJsfX0se2tleTpcInNldFByZWZpeFwiLHZhbHVlOmZ1bmN0aW9uKGUpe3RoaXMuY29udGV4dF8ucHJlZml4PWV9fSx7a2V5Olwic2V0U2NoZW1lXCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5jb250ZXh0Xy5zY2hlbWU9ZX19LHtrZXk6XCJnZXRTY2hlbWVcIix2YWx1ZTpmdW5jdGlvbigpe3JldHVybiB0aGlzLmNvbnRleHRfLnNjaGVtZX19LHtrZXk6XCJzZXRIb3N0XCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5jb250ZXh0Xy5ob3N0PWV9fSx7a2V5OlwiZ2V0SG9zdFwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29udGV4dF8uaG9zdH19LHtrZXk6XCJzZXRQb3J0XCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5jb250ZXh0Xy5wb3J0PWV9fSx7a2V5OlwiZ2V0UG9ydFwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29udGV4dF8ucG9ydH19LHtrZXk6XCJzZXRMb2NhbGVcIix2YWx1ZTpmdW5jdGlvbihlKXt0aGlzLmNvbnRleHRfLmxvY2FsZT1lfX0se2tleTpcImdldExvY2FsZVwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29udGV4dF8ubG9jYWxlfX0se2tleTpcImJ1aWxkUXVlcnlQYXJhbXNcIix2YWx1ZTpmdW5jdGlvbihlLHQsbyl7dmFyIHI9dGhpcyxpPXZvaWQgMCx1PW5ldyBSZWdFeHAoL1xcW1xcXSQvKTtpZih0IGluc3RhbmNlb2YgQXJyYXkpdC5mb3JFYWNoKGZ1bmN0aW9uKHQsaSl7dS50ZXN0KGUpP28oZSx0KTpyLmJ1aWxkUXVlcnlQYXJhbXMoZStcIltcIisoXCJvYmplY3RcIj09PShcInVuZGVmaW5lZFwiPT10eXBlb2YgdD9cInVuZGVmaW5lZFwiOm4odCkpP2k6XCJcIikrXCJdXCIsdCxvKX0pO2Vsc2UgaWYoXCJvYmplY3RcIj09PShcInVuZGVmaW5lZFwiPT10eXBlb2YgdD9cInVuZGVmaW5lZFwiOm4odCkpKWZvcihpIGluIHQpdGhpcy5idWlsZFF1ZXJ5UGFyYW1zKGUrXCJbXCIraStcIl1cIix0W2ldLG8pO2Vsc2UgbyhlLHQpfX0se2tleTpcImdldFJvdXRlXCIsdmFsdWU6ZnVuY3Rpb24oZSl7dmFyIHQ9dGhpcy5jb250ZXh0Xy5wcmVmaXgrZSxuPWUrXCIuXCIrdGhpcy5jb250ZXh0Xy5sb2NhbGUsbz10aGlzLmNvbnRleHRfLnByZWZpeCtlK1wiLlwiK3RoaXMuY29udGV4dF8ubG9jYWxlLHI9W3QsbixvLGVdO2Zvcih2YXIgaSBpbiByKWlmKHJbaV1pbiB0aGlzLnJvdXRlc18pcmV0dXJuIHRoaXMucm91dGVzX1tyW2ldXTt0aHJvdyBuZXcgRXJyb3IoJ1RoZSByb3V0ZSBcIicrZSsnXCIgZG9lcyBub3QgZXhpc3QuJyl9fSx7a2V5OlwiZ2VuZXJhdGVcIix2YWx1ZTpmdW5jdGlvbihlLG4pe3ZhciBvPWFyZ3VtZW50cy5sZW5ndGg+MiYmdm9pZCAwIT09YXJndW1lbnRzWzJdJiZhcmd1bWVudHNbMl0saT10aGlzLmdldFJvdXRlKGUpLHU9bnx8e30scz10KHt9LHUpLGM9XCJcIixhPSEwLGw9XCJcIixmPVwidW5kZWZpbmVkXCI9PXR5cGVvZiB0aGlzLmdldFBvcnQoKXx8bnVsbD09PXRoaXMuZ2V0UG9ydCgpP1wiXCI6dGhpcy5nZXRQb3J0KCk7aWYoaS50b2tlbnMuZm9yRWFjaChmdW5jdGlvbih0KXtpZihcInRleHRcIj09PXRbMF0pcmV0dXJuIGM9ci5lbmNvZGVQYXRoQ29tcG9uZW50KHRbMV0pK2Msdm9pZChhPSExKTt7aWYoXCJ2YXJpYWJsZVwiIT09dFswXSl0aHJvdyBuZXcgRXJyb3IoJ1RoZSB0b2tlbiB0eXBlIFwiJyt0WzBdKydcIiBpcyBub3Qgc3VwcG9ydGVkLicpO3ZhciBuPWkuZGVmYXVsdHMmJnRbM11pbiBpLmRlZmF1bHRzO2lmKCExPT09YXx8IW58fHRbM11pbiB1JiZ1W3RbM11dIT1pLmRlZmF1bHRzW3RbM11dKXt2YXIgbz12b2lkIDA7aWYodFszXWluIHUpbz11W3RbM11dLGRlbGV0ZSBzW3RbM11dO2Vsc2V7aWYoIW4pe2lmKGEpcmV0dXJuO3Rocm93IG5ldyBFcnJvcignVGhlIHJvdXRlIFwiJytlKydcIiByZXF1aXJlcyB0aGUgcGFyYW1ldGVyIFwiJyt0WzNdKydcIi4nKX1vPWkuZGVmYXVsdHNbdFszXV19dmFyIGw9ITA9PT1vfHwhMT09PW98fFwiXCI9PT1vO2lmKCFsfHwhYSl7dmFyIGY9ci5lbmNvZGVQYXRoQ29tcG9uZW50KG8pO1wibnVsbFwiPT09ZiYmbnVsbD09PW8mJihmPVwiXCIpLGM9dFsxXStmK2N9YT0hMX1lbHNlIG4mJnRbM11pbiBzJiZkZWxldGUgc1t0WzNdXX19KSxcIlwiPT09YyYmKGM9XCIvXCIpLGkuaG9zdHRva2Vucy5mb3JFYWNoKGZ1bmN0aW9uKGUpe3ZhciB0PXZvaWQgMDtyZXR1cm5cInRleHRcIj09PWVbMF0/dm9pZChsPWVbMV0rbCk6dm9pZChcInZhcmlhYmxlXCI9PT1lWzBdJiYoZVszXWluIHU/KHQ9dVtlWzNdXSxkZWxldGUgc1tlWzNdXSk6aS5kZWZhdWx0cyYmZVszXWluIGkuZGVmYXVsdHMmJih0PWkuZGVmYXVsdHNbZVszXV0pLGw9ZVsxXSt0K2wpKX0pLGM9dGhpcy5jb250ZXh0Xy5iYXNlX3VybCtjLGkucmVxdWlyZW1lbnRzJiZcIl9zY2hlbWVcImluIGkucmVxdWlyZW1lbnRzJiZ0aGlzLmdldFNjaGVtZSgpIT1pLnJlcXVpcmVtZW50cy5fc2NoZW1lKXt2YXIgaD1sfHx0aGlzLmdldEhvc3QoKTtjPWkucmVxdWlyZW1lbnRzLl9zY2hlbWUrXCI6Ly9cIitoKyhoLmluZGV4T2YoXCI6XCIrZik+LTF8fFwiXCI9PT1mP1wiXCI6XCI6XCIrZikrY31lbHNlIGlmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiBpLnNjaGVtZXMmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiBpLnNjaGVtZXNbMF0mJnRoaXMuZ2V0U2NoZW1lKCkhPT1pLnNjaGVtZXNbMF0pe3ZhciBwPWx8fHRoaXMuZ2V0SG9zdCgpO2M9aS5zY2hlbWVzWzBdK1wiOi8vXCIrcCsocC5pbmRleE9mKFwiOlwiK2YpPi0xfHxcIlwiPT09Zj9cIlwiOlwiOlwiK2YpK2N9ZWxzZSBsJiZ0aGlzLmdldEhvc3QoKSE9PWwrKGwuaW5kZXhPZihcIjpcIitmKT4tMXx8XCJcIj09PWY/XCJcIjpcIjpcIitmKT9jPXRoaXMuZ2V0U2NoZW1lKCkrXCI6Ly9cIitsKyhsLmluZGV4T2YoXCI6XCIrZik+LTF8fFwiXCI9PT1mP1wiXCI6XCI6XCIrZikrYzpvPT09ITAmJihjPXRoaXMuZ2V0U2NoZW1lKCkrXCI6Ly9cIit0aGlzLmdldEhvc3QoKSsodGhpcy5nZXRIb3N0KCkuaW5kZXhPZihcIjpcIitmKT4tMXx8XCJcIj09PWY/XCJcIjpcIjpcIitmKStjKTtpZihPYmplY3Qua2V5cyhzKS5sZW5ndGg+MCl7dmFyIGQ9dm9pZCAwLHk9W10sdj1mdW5jdGlvbihlLHQpe3Q9XCJmdW5jdGlvblwiPT10eXBlb2YgdD90KCk6dCx0PW51bGw9PT10P1wiXCI6dCx5LnB1c2goci5lbmNvZGVRdWVyeUNvbXBvbmVudChlKStcIj1cIityLmVuY29kZVF1ZXJ5Q29tcG9uZW50KHQpKX07Zm9yKGQgaW4gcyl0aGlzLmJ1aWxkUXVlcnlQYXJhbXMoZCxzW2RdLHYpO2M9YytcIj9cIit5LmpvaW4oXCImXCIpfXJldHVybiBjfX1dLFt7a2V5OlwiZ2V0SW5zdGFuY2VcIix2YWx1ZTpmdW5jdGlvbigpe3JldHVybiBpfX0se2tleTpcInNldERhdGFcIix2YWx1ZTpmdW5jdGlvbihlKXt2YXIgdD1yLmdldEluc3RhbmNlKCk7dC5zZXRSb3V0aW5nRGF0YShlKX19LHtrZXk6XCJjdXN0b21FbmNvZGVVUklDb21wb25lbnRcIix2YWx1ZTpmdW5jdGlvbihlKXtyZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGUpLnJlcGxhY2UoLyUyRi9nLFwiL1wiKS5yZXBsYWNlKC8lNDAvZyxcIkBcIikucmVwbGFjZSgvJTNBL2csXCI6XCIpLnJlcGxhY2UoLyUyMS9nLFwiIVwiKS5yZXBsYWNlKC8lM0IvZyxcIjtcIikucmVwbGFjZSgvJTJDL2csXCIsXCIpLnJlcGxhY2UoLyUyQS9nLFwiKlwiKS5yZXBsYWNlKC9cXCgvZyxcIiUyOFwiKS5yZXBsYWNlKC9cXCkvZyxcIiUyOVwiKS5yZXBsYWNlKC8nL2csXCIlMjdcIil9fSx7a2V5OlwiZW5jb2RlUGF0aENvbXBvbmVudFwiLHZhbHVlOmZ1bmN0aW9uKGUpe3JldHVybiByLmN1c3RvbUVuY29kZVVSSUNvbXBvbmVudChlKS5yZXBsYWNlKC8lM0QvZyxcIj1cIikucmVwbGFjZSgvJTJCL2csXCIrXCIpLnJlcGxhY2UoLyUyMS9nLFwiIVwiKS5yZXBsYWNlKC8lN0MvZyxcInxcIil9fSx7a2V5OlwiZW5jb2RlUXVlcnlDb21wb25lbnRcIix2YWx1ZTpmdW5jdGlvbihlKXtyZXR1cm4gci5jdXN0b21FbmNvZGVVUklDb21wb25lbnQoZSkucmVwbGFjZSgvJTNGL2csXCI/XCIpfX1dKSxyfSgpO3IuUm91dGUsci5Db250ZXh0O3ZhciBpPW5ldyByO3JldHVybntSb3V0ZXI6cixSb3V0aW5nOml9fSk7Il0sInNvdXJjZVJvb3QiOiIifQ==