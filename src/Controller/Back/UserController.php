<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 12/06/20
 * Time: 21:08
 */

namespace App\Controller\Back;


use App\Entity\Users;
use App\Form\Back\UsersType;
use App\Form\Back\UserUpdateType;
use App\Mailer\Mailer;
use App\Repository\UsersRepository;
use App\Service\Users\UserService;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserController
 * @package App\Controller\Back
 * @Route("/users", name="backUser_")
 */
class UserController extends AbstractController
{

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;


    public function __construct(UsersRepository $usersRepository, TranslatorInterface $translator)
    {
        $this->usersRepository = $usersRepository;
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Mailer $mailer
     * @Route("/create", name="create")
     */
    public function createUser(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        Mailer $mailer
    ): Response
    {
        //$this->denyAccessUnlessGranted('backUser_create');
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = bin2hex(random_bytes(4));
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $password
                )
            );

            $user->setIsVerified(false);
            $user->setConfirmationToken(bin2hex(random_bytes(24)));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $mailer->sendInvitation($user, $password);

            $msg = $this->translator->trans('User admin create successfull!', [ '%identifier%' => $user, ], 'back_messages');
            $this->addFlash('success', $msg);
            return $this->redirectToRoute('backUser_list');
        }

        return $this->render('back/user/create_user.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Users $users
     * @return Response
     * @Route("/list", name="list")
     */
    public function readUser(UserService $userService)
    {
        $users = $userService->getAllUser();
        return $this->render('back/user/list_user.html.twig', compact('users'));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteUser($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(Users::class)->find($id);
        if (!$user) {
            throw $this->createNotFoundException(
                'There are no user in the following id: ' . $id
            );
        }
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->redirectToRoute("backUser_list");
    }

    /**
     * @param $id
     * @return Response
     * @Route("/read/{id}", name="read")
     */
    public function getUserById($id)
    {
        $users = $this->getDoctrine()->getRepository(Users::class)->find($id);
        return $this->render('back/user/read.html.twig', compact('users'));
    }

    /**
     * @param Request $request
     * @param Users $users
     * @Route("/update/{id}", name="update")
     */
    public function updateUser(Request $request, Users $users)
    {
        $form = $this->createForm(UserUpdateType::class, $users);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $msg = $this->translator->trans('User admin Update successfull!', [ '%identifier%' => $users, ], 'back_messages');
            $this->addFlash('success', $msg);
            return $this->redirectToRoute('backUser_list');
        }
        return $this->render('back/user/update_user.html.twig', [
            'user' => $users,
            'form' => $form->createView(),
        ]);
    }
}