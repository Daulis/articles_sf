<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 14/06/20
 * Time: 21:38
 */

namespace App\Service\Product;



use App\Repository\Product\ProductRepository;

class ProductService
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll()
    {
        $product = $this->productRepository->findBy([], ['createdAt' => 'desc']);
        return $product;
    }
}