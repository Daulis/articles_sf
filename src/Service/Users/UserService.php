<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 12/06/20
 * Time: 22:19
 */

namespace App\Service\Users;


use App\Repository\UsersRepository;

class UserService
{
    protected $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function getAllUser()
    {
        $users = $this->usersRepository->findBy([], ['createdAt' => 'desc']);
        return $users;
    }
}