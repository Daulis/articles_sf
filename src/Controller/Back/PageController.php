<?php

namespace App\Controller\Back;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/backOffice", name="backOffice_")
     */
    public function index()
    {
        return $this->render('back/page/index.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }
}
