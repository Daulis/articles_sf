<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="frontOffice_")
     */
    public function index()
    {
        return $this->render('front/page/index.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }
}
