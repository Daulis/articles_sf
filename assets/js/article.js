const $ = require('jquery');
const routes = require('../../public/js/fos_js_routes');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);
console.log(Routing.generate('actualites_articles'));

const axios = require('axios');
const $articleContainer = $('#article-container');
function showArticles(data) {
    data.forEach(article => {
        let newContent = `<h1><a href="${article.url}">${article.title}</a></h1><p>${article.content}</p>`;
        $articleContainer.append(newContent);
    });
}

function downloadArticles() {
    $.ajax({
        type: 'GET',
        url: Routing.generate('actualites_hello'),
        dataType: 'json',
        success: function(resp){
            showArticles(resp);
        },
        error: function () {
            console.log('error');
        }
    })
}

$(document).ready(function () {
   /*$('#btn-show-article').on('click', function () {
       downloadArticles();
   })*/
   downloadArticles();
});