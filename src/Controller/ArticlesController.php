<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Commentaires;
use App\Form\CommentairesType;
use phpDocumentor\Reflection\Types\This;
use App\Repository\ArticlesRepository;
use App\Service\ArticlesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class ArticlesController
 * @package App\Controller
 * @Route("/actualites", name="actualites_")
 */
class ArticlesController extends AbstractController
{
    /**
     * @Route("/", options={"expose"=true}, name="product")
     * @param ArticlesRepository $articlesRepository
     */
    public function index(ArticlesRepository $articlesRepository, ArticlesService $articlesService)
    {
        $articles = $articlesService->getAllArticle();
        return $this->render('back/product/index.html.twig', compact('articles'));
        dd($articles);
    }

    /**
     * @Route("/hello", name="hello", options={"expose"=true})
     * @param ArticlesRepository $articlesRepository
     * @return mixed
     */
    public function hello(ArticlesRepository $articlesRepository)
    {
        $res = [];
        $articles = $articlesRepository->findBy([], ['created_at' => 'desc']);
        foreach ($articles as $article) {
            $res[] = [
                'title' => $article->getTitre(),
                'content' => $article->getContenu(),
                'url' => $this->generateUrl('actualites_article', ['slug' => $article->getSlug()])
            ];
        }
        return $this->json($res);
    }

    /**
     * @Route("/{slug}", name="article")
     */
    public function article($slug, Request $request, ArticlesService $articlesService)
    {
        $article = $articlesService->getArticleBySlug($slug);

        $commentaires = $this->getDoctrine()->getRepository(Commentaires::class)->findBy([
            'product'=>$article,
            'actif'=> 1
        ],['created_at'=> 'desc']);

        if(!$article) {
            throw $this->createNotFoundException('L\'article ne peut pas être trouve');
        }

        $commentaire = new Commentaires();
        $form = $this->createForm(CommentairesType::class, $commentaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $commentaire->setArticles($article);
            $commentaire->setCreatedAt(new \DateTime('now'));
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($commentaire);
            $doctrine->flush();
            return  $this->redirectToRoute('actualites_article', ['slug'=> $slug]);
        }

        return $this->render(
            'back/product/product.html.twig', [
                'form' => $form->createView(),
                'article' => $article,
                'commentaires' => $commentaires
            ]);
    }
}
