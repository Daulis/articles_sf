<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BackProductController extends AbstractController
{
    /**
     * @Route("/back/product", name="back_product")
     */
    public function index()
    {
        return $this->render('back_product/index.html.twig', [
            'controller_name' => 'BackProductController',
        ]);
    }
}
