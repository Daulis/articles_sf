<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 14/06/20
 * Time: 14:42
 */

namespace App\Controller\Back\Categorie;


use App\Entity\Categorie\Categories;
use App\Form\Back\Categorie\CategoriesType;
use App\Service\Categorie\CategorieService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategorieController
 * @package App\Controller\Back\Categorie
 * @Route("/categorie", name="categories_")
 */
class CategorieController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/create", name="create")
     */
    public function createCategorie(Request $request): Response
    {
        $categorie = new Categories();
        $form = $this->createForm(CategoriesType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $request->files->get('categories')['categorieFile'];
            $uploads_directory = $this->getParameter('uploads_directory');

            $fileName = md5(uniqid()).'.'. $file->guessExtension();
            $file->move(
                $uploads_directory,
                $fileName
            );
            //dd($file);
            $em = $this->getDoctrine()->getManager();
            $categorie->setCategorieFile($fileName);
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('categories_list');
        }
        return $this->render('back/categorie/create_categorie.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @return Response
     * @Route("/list", name="list")
     */
    public function getAllCategorie(CategorieService $categorieService): Response
    {
        $categories = $categorieService->getAll();
        return $this->render('back/categorie/list_categorie.html.twig', compact('categories'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/edit/{id}", name="edit")
     */
    public function updateCategorie(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository(Categories::class)->find($id);
        $form = $this->createForm(CategoriesType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $request->files->get('categories')['categorieFile'];
            $uploads_directory = $this->getParameter('uploads_directory');

            $fileName = md5(uniqid()).'.'. $file->guessExtension();
            $file->move(
                $uploads_directory,
                $fileName
            );
            $categorie->setCategorieFile($fileName);
            $em->flush();
            return $this->redirectToRoute('categories_list');
        }
        return $this->render('back/categorie/create_categorie.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @param $id
     * @return Response
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteCategorie($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository(Categories::class)->find($id);
        $em->remove($categorie);
        $em->flush();
        return $this->redirectToRoute('categories_list');
    }

    /**
     * @param $id
     * @return Response
     * @Route("/read/{id}", name="read")
     */
    public function categorieById($id)
    {
        $categorie = $this->getDoctrine()->getRepository(Categories::class)->find($id);
        return $this->render('back/categorie/_categorie.html.twig', compact('categorie'));
    }
}