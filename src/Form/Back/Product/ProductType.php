<?php

namespace App\Form\Back\Product;

use App\Entity\Product\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du produit'
            ])
            ->add('color', TextType::class, [
                'label' => 'Couleur du produit'
            ])
            ->add('price', IntegerType::class, [
                'label' => 'Combien voulez-vous le vendre ?'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description du produit'
            ])
            ->add('categorie', EntityType::class, [
                'class' => 'App\Entity\Categorie\Categories',
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false
            ])
            ->add('productFiles', FileType::class, [
                'mapped' => false,
                'label' => 'Choose',
                'multiple' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
