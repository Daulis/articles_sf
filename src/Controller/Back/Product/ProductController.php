<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 14/06/20
 * Time: 21:33
 */

namespace App\Controller\Back\Product;


use App\Entity\Product\Product;
use App\Form\Back\Product\ProductType;
use App\Repository\Product\ProductRepository;
use App\Service\Product\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller\Back\Product
 * @Route("/product", name="product_")
 */
class ProductController extends AbstractController
{

    /**
     * @param Request $request
     * @return Response
     * @Route("/create", name="create")
     */
    public function addProduct(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploads_directory = $this->getParameter('uploads_directory');
            $em = $this->getDoctrine()->getManager();
            $files = $request->files->get('product')['productFiles'];

            foreach ($files as $file) {
                $filesName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $uploads_directory,
                    $filesName
                );
            }

            //dd($files);
            $product->setProductFiles($filesName);
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_list');
        }
        return $this->render('back/product/create_product.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @return Response
     * @Route("/list", name="list")
     */
    public function getAllProduct(ProductService $productService): Response
    {
        $products = $productService->getAll();
        return $this->render('back/product/index.html.twig', compact('products'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/update/{id}", name="update")
     */
    public function updateProduct(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('product_list');
        }
        return $this->render('back/product/create_product.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteProduct($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('product_list');
    }

    /**
     * @param $id
     * @return Response
     * @Route("/read/{id}", name="read")
     */
    public function productById($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        return $this->render('back/product/show_product.html.twig', compact('product'));
    }
}