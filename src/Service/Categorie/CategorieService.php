<?php
/**
 * Created by PhpStorm.
 * User: daulis
 * Date: 14/06/20
 * Time: 16:13
 */

namespace App\Service\Categorie;


use App\Repository\Categorie\CategoriesRepository;

class CategorieService
{
    protected $categorieRepository;

    public function __construct(CategoriesRepository $categorieRepository)
    {
        $this->categorieRepository = $categorieRepository;
    }

    public function getAll()
    {
        $categorie = $this->categorieRepository->findBy([], ['createdAt' => 'desc']);
        return $categorie;
    }

}